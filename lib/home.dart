import 'package:flutter/material.dart';
import 'package:lern_app_langsteiner/newEntry.dart';
import 'fach.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Fach> faecher = <Fach>[
    Fach("AM", 0),
    Fach("POS", 0),
    Fach("GGP", 0),
    Fach("NVS", 0)
  ];
  List<Addition> additions = <Addition>[];
  int sum = 0;





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My studies"),
        actions: [
          IconButton(
              onPressed: () {
                showModalBottomSheet(
                  context: context,
                  builder: (context) {return NewEntry(setterForState: setNewEntry,);},
                );
              },
              icon: Icon(Icons.add)
          )
        ],
      ),

      body:
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //_fillBarsDisplay(),
              _additionsDisplay()
            ],
          ),
        ),

    );
  }


  _additionsDisplay() {
    return Column(
      children:
        additions.map(
                (e) => _additionDisplay(e))
            .toList()
    );
  }


  Widget _additionDisplay(Addition addition) {
    return ListTile(
      leading: Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent)
        ),
        child: Text("${addition.amount}"),
      ),
      title: Text(addition.fach.name),
      subtitle: Text("${addition.date}"),
      trailing: IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () {
          setState(() {
            additions.remove(addition);
          });
        },
      ),
    );
  }

  void setNewEntry(Addition addition) {
    setState(() {
      additions.add(addition);
      sum += addition.amount;
      faecher.where((x) {return x.name == addition.fach.name;}).first.total += addition.amount;
    });
  }


  Widget FillBar(Fach fach) {
    return Padding(padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 80,
        width: 40,

        child: Column(
          children: [
            Text(fach.name),
            Text(fach.total.toString()),
            RotatedBox(
              quarterTurns: 3,
              child: LinearProgressIndicator(
                value: 0.5,
                backgroundColor: Colors.transparent,
                valueColor:
                const AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
            ),
          ],
        )



      )
    );
  }

  _fillBarsDisplay() {
    return Row(
        children:
        faecher.map(
                (e) => FillBar(e))
            .toList()
    );
  }
}