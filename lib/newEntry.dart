import 'package:flutter/material.dart';
import 'package:lern_app_langsteiner/fach.dart';

class NewEntry extends StatefulWidget {
  const NewEntry({Key? key, required this.setterForState}) : super(key: key);

  final Function setterForState;
  @override
  State<NewEntry> createState() => _NewEntryState();
}

class _NewEntryState extends State<NewEntry> {
  List<String> faecher = ["AM", "POS", "GGP", "NVS"];
  String fach_dropdown_value = "AM";
  int hours = 0;
  TextEditingController hoursController = TextEditingController();
  DateTime selectedDate = DateTime.now();

  

  @override
  Widget build(BuildContext context) {
    hoursController.addListener(() {
      if (isNumeric(hoursController.text)){
        setState(() {
          hours = int.parse(hoursController.text);
        });
        print(hours);
      }
    });

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        DropdownButton<String>(
            value: fach_dropdown_value,
            items: faecher.map((String items) {
              return DropdownMenuItem(
                value: items,
                child: Text(items),
              );
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                fach_dropdown_value = newValue.toString();
              });
            }
        ),

        TextField(
          decoration: InputDecoration(
            labelText: 'Amount',
            hintText: 'The amount of hours used for subject'
          ),
          controller: hoursController,
        ),

        TextButton(
          onPressed: () async {
            final DateTime? selected = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(2020, 1),
              lastDate: DateTime(2201),
            );
            if (selected != null && selected != selectedDate)
              setState(() {
                selectedDate = selected;
              });
            print(selected);
          },
          child: Text("Date"),
        ),



        IconButton(
          onPressed: () {
            Addition newAddition = Addition(Fach(fach_dropdown_value, 0), hours, selectedDate);
            print(newAddition.fach.name);
            print(newAddition.amount);
            print(newAddition.date);
            widget.setterForState(newAddition);
          },
          icon: Icon(Icons.add),
        )
      ],
    );
  }

  bool isNumeric(String str) {
    try{
      int.parse(str);
    } on FormatException {
      return false;
    } finally {
      return true;
    }
  }
}