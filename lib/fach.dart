class Fach {
  final String name;
  int total;

  Fach(this.name, this.total);
}


class Addition {
  final Fach fach;
  final int amount;
  final DateTime date;

  Addition(this.fach, this.amount, this.date);
}